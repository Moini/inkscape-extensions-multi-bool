=======
Licence
=======
This Inkscape extension is licensed as GPLv2 or later (see also https://github.com/ryanlerch/inkscape-extension-multiple-difference/issues/1#issuecomment-178285798)
